# Introduction to Linux

This directory contains slides (PDFs) that help you get started on a Linux operating system, which should help you find your way on the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)).

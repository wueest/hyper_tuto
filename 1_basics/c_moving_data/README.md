# A Hyperion Tutorial

This directory contains information (in the form of PDFs) on how to move data to and from the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)).

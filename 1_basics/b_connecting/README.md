# Connect to hyperion.wsl.ch

This directory contains a presentation on how to connect to the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)) via ssh on multiple operating systems. Slides are provided as PDF.
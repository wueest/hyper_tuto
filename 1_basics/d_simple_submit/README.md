# Submit a simple job

This repository contains information on how to run a very basix example script (in this case calling the R software) to run a job on the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)).

You find a PDF of the slides presented during the workshop, as well as the two example scripts to generate some random numbers on hyperion.

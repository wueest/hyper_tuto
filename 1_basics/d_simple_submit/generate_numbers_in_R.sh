#!/bin/bash

#SBATCH -A node # Node account
#SBATCH -p node # Node partition
#SBATCH --qos normal  # normal priority level

#SBATCH --job-name=gen_rnum
#SBATCH --output=gen_rnum_%j.out
#SBATCH --time=00:02:00
#SBATCH --chdir=/home/wueest/

#SBATCH --ntasks=1

Rscript ~/hyper_tuto/1_basics/d_simple_submit/generate_numbers.r

OUT=~/hyper_tuto/1_basics/d_simple_submit/randomnumbers.txt

cp ~/randomnumbers.txt $OUT

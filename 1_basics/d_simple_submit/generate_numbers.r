# generate random numbers
numbers <- runif(n = 100, min = 0, max = 10)

# export them
write.table(numbers, 'randomnumbers.txt',
  eol = '\t', col.names = FALSE, row.names = FALSE)

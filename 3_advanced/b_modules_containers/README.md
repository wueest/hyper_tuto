# Modules and Containers

This directory contains material that helps you to work with modules and containers on the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)).

It contains two PDFs that introduce [modules](3_advanced/b_modules_containers/modules.pdf) and [containers](3_advanced/b_modules_containers/Singularity_containers.pdf).

It also contains a container [definition file (also called recipe)](3_advanced/b_modules_containers/Singularity.recipe) that illustrates how containers can be build using a few lines, and an [`sbatch` script](3_advanced/b_modules_containers/example_slurm.script) that shows how to use a container when running your job on hyperion.

Last but not least, you can find a [script that installs singularity on your local Linux computer](3_advanced/b_modules_containers/install.singularity). This script is not updated nor continuously tested so might need slight adjustments. Please constult the [singluarity documentation](https://sylabs.io/guides/master/user-guide/) if in doubt.

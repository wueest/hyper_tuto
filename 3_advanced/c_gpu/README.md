# Using GPUs on hyperion

This directory contains slides to show how to run jobs that use GPUs on the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)). You also find associated scripts.

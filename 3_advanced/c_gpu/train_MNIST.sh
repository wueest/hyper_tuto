#!/bin/sh
#SBATCH --job-name=mnist
#SBATCH --partition=gpu
#SBATCH --account=gpu
#SBATCH --mem=5G
#SBATCH --gres=gpu:1
#SBATCH --nodelist=gpunode[01]
#SBATCH --time=0-01:0:00
#SBATCH --output=mnist_%j.out

Rscript /home/rehush/train_MNIST.R

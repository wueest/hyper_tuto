# This script creates different numbers of files
# writes them onto a drive and records the time needed

sizes <- c(10, 100, 1000, 10000, 100000,1000000)
for (drive in c('/storage','/home'))
{
time <- c()
for (size in sizes)
{
    print(size)
    tmp <- paste0(drive,'/karger/scratch/test/')
    system(paste0('mkdir ',tmp))
    time_many_start <-  Sys.time()
    for (n in 1:size)
        {
            m <- 1
            write.table(m, file = paste0(tmp,m,'.txt'))
        }
    time_many_end <-  Sys.time()
    system(paste0('rm -r ',tmp))
    time <- c(time, time_many_end - time_many_start)
}
assign(paste0(gsub('/','',drive),'time'),time)
}
storagetime[5]<-storagetime[5]*60 #convert minutes to seconds
hometime[5]<-hometime[5]*60 #convert minutes to seconds

storagetime[6]<-storagetime[6]*60 #convert minutes to seconds
hometime[6]<-hometime[6]*60 #convert minutes to seconds

pdf('/storage/karger/sizes.pdf', height=5, width=5)
plot(sizes, storagetime, col='red', type="l", ylab="time [s]", xlab="number of files")
points(sizes, hometime, col='darkgreen', type="l")
dev.off()
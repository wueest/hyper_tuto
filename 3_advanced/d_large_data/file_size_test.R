# this script creates 1MB of data and writes it out as either 1 file, or 1'000 files
# it checks the time how long it takes to write out the files to different drives

for (drive in c('/storage','/home'))
{
    time_many <- c()
    time_one <- c()
    for (nn in 1:10)
    {
        # run the test for many files
        print(nn)
        tmp <- paste0(drive,'/karger/scratch/test/')
        system(paste0('mkdir ',tmp))
        time_many_start <-  Sys.time()
        for (n in 1:1000)
        {
            m <- 1
            write.table(m, file = paste0(tmp,m,'.txt'))
        }
        time_many_end <-  Sys.time()
        system(paste0('rm -r ',tmp))
        time_many <- c(time_many, time_many_end - time_many_start)

        # run the test for one files
        system(paste0('mkdir ',tmp))
        m <- rep(1,1000)
        time_one_start <- Sys.time()
        write.table(m, file = paste0(tmp,'m.txt'))
        time_one_end <- Sys.time()
        system(paste0('rm -r ',tmp))
        time_one <- c(time_one, time_one_end - time_one_start)
}
df1 <- cbind(time_one, time_many)
pdf(paste0('/storage/karger/plot_',gsub('/','',drive),'.pdf'), height=5, width=5)
boxplot(df1, col = c('blue','red'), ylab="time [s]", main = drive, ylim = c(0,4))
dev.off()
}

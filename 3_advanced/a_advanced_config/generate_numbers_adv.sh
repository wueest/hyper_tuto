#!/bin/bash

#SBATCH --job-name=rnum_adv
#SBATCH --output=rnum_adv_%A_%a.out

#SBATCH --array=1-10
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10

Rscript ~/hyper_tuto/3_advanced/a_advanced_config/generate_numbers_adv.r

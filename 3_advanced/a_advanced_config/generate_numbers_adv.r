# setup
require(parallel)
alloc_cpus <- as.numeric(Sys.getenv('SLURM_CPUS_PER_TASK'))
array_id <- as.numeric(Sys.getenv('SLURM_ARRAY_TASK_ID'))

# generate random numbers in parallel
numbers_list <- mclapply(1:10, function(i) {
  runif(n = 10, min = 0, max = 10)
}, mc.cores = alloc_cpus)
numbers <- unlist(numbers_list)

# export
fnam <- paste0('rnumbers_adv_', array_id, '.txt')
cat('repetition ', array_id, ':\n', sep = '', file = fnam)
cat(numbers, file = fnam, append = TRUE)

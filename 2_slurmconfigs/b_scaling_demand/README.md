# Scaling demand
This directory contains scripts that can be used to illustrate how much time, CPU, memory certain (in this case simple) tasks require. They can all be run (with slight adjustments of paths) be run on the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)).

The results of the exercise are contained in the PDF taht can be found here: [2_slurmconfigs/d_mem_configs/Memory_managment.pdf](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/blob/master/2_slurmconfigs/d_mem_configs/Memory_managment.pdf).

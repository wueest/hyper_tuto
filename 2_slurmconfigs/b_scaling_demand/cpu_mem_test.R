cpu_mem_test <- function(n, m, dir, ...){
    mcluster <- makeCluster(m, type = "PSOCK") #create a cluster of m cpus
    registerDoParallel(mcluster) # register a parallel backend
    m1 <- matrix(2, n, n) # create a matrix with n rows and n columns containing n*n 2s
    time_start <- Sys.time()# collect the current time
    x <- foreach(i=1:length(m1[1,]), .combine = 'c') %dopar% sqrt(m1[i,])# run a calculation on the matrix using m cpus
    x_size <- object.size(x)# collect the object size
    time_end_calc <- Sys.time() # collect the current time
    write.table(x, file = paste0(dir, 'out.txt'))# write out results
    time_end_io <- Sys.time()# collect the current time
    system(paste0('rm ',dir,'out.txt'))# delete the output
    df_out <- data.frame(starttime = time_start, 
                    calculation_time = as.numeric(time_end_calc - time_start), 
                    io_time = as.numeric(time_end_io - time_start), 
                    memorysize_bytes = as.numeric(x_size),
                    matrixsize = n*n,
                    cpus = m)# create a dataframe for output
    stopCluster(cl = mcluster)
    return(df_out)
}

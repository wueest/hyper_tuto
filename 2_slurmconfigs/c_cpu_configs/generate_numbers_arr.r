# setup
array_id <- as.numeric(Sys.getenv('SLURM_ARRAY_TASK_ID'))

# generate random numbers
numbers <- runif(n = 10, min = 0, max = 10)

# export
fnam <- paste0('rnumbers_arr_', array_id, '.txt')
cat('iteration', array_id, ':', numbers, '\n', file = fnam)

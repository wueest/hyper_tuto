# setup
require(parallel)
alloc_cpus <- as.numeric(Sys.getenv('SLURM_CPUS_PER_TASK'))

# generate random numbers in parallel
t1 <- Sys.time()
numbers <- mclapply(1:10, function(i) {
  # pause for 3 seconds (check if parallel setup works)
  Sys.sleep(3)
  # random numbers
  runif(n = 10, min = 0, max = 10)
}, mc.cores = alloc_cpus)
tdiff <- as.numeric(Sys.time() - t1)

# export
write.table(unlist(numbers), 'rnumbers_par.txt',
  eol = '\t', col.names = FALSE, row.names = FALSE)
cat('\n job completed in', tdiff, 'seconds\n',
  file = 'rnumbers_par.txt', append = TRUE)

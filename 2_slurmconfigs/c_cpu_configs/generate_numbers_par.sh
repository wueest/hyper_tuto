#!/bin/bash

#SBATCH -A node
#SBATCH -p node
#SBATCH --job-name=rnum_par
#SBATCH --output=rnum_par_%j.out
#SBATCH --time=00:02:00
#SBATCH --chdir=$HOME
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10

Rscript ~/hyper_tuto/2_slurmconfigs/c_cpu_config/generate_numbers_par.r

OUT=~/hyper_tuto/2_slurmconfigs/c_cpu_config/rnumbers_par.txt
cp ~/rnumbers_par.txt $OUT

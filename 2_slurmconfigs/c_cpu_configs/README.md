# Configuring CPU

This repository contains information on how to configure CPU demand through `sbatch` options on the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)).

You find a PDF of the slides presented during the workshop, as well as the two examples to generate some random numbers on hyperion in parallel. Both examples call a simple R-script to generate random numbers.

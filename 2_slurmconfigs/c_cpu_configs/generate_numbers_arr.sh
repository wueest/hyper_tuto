#!/bin/bash

#SBATCH -A node
#SBATCH -p node
#SBATCH --job-name=rnum_par
#SBATCH --output=rnum_arr_%A_%a.out
#SBATCH --time=00:02:00
#SBATCH --chdir=$HOME

#SBATCH --array=1-10:1%10
#SBATCH --ntasks=1

Rscript ~/hyper_tuto/2_slurmconfigs/c_cpu_config/generate_numbers_arr.r

OUT=~/hyper_tuto/2_slurmconfigs/c_cpu_config/
cat ~/rnumbers_arr_$SLURM_ARRAY_TASK_ID.txt >> $OUT/rnumbers_arr.txt
rm ~/rnumbers_arr_$SLURM_ARRAY_TASK_ID.txt

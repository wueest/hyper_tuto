# A Hyperion Tutorial

This repository contains examples to run jobs on the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)).

The examples are usually calling R-Scripts but can be adopted to run any job/software available on hyperion.

The scripts are organised in folders that follow the  [presentation](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/blob/master/hyperion_tutorial.pdf) that is also available.

Furthermore, we provide [install_singularity.sh](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/blob/master/install_singularity.sh), a suit of commands that help installing singularity (e.g. on ubuntu). Be aware that we do not update the script to keep track of the newest versions. When in doubt, follow the installation instructions in [https://sylabs.io/guides/3.7/user-guide/quick_start.html#quick-installation-steps](https://sylabs.io/guides/3.7/user-guide/quick_start.html#quick-installation-steps).
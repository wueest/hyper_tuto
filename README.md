# A Hyperion Tutorial

This repository contains examples to run jobs on the WSL hyperion cluster ([hyperion.wsl.ch](https://hyperion.wsl.ch)).

The examples are usually calling R-Scripts but can be adopted to run any job/software available on hyperion.

The folder structure follows the outline of the workshop contents:
1. [The Basics](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/1_basics)
* [Intro on hyperion](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/1_basics/0_intro_hyperion)
* [Basic Linux](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/1_basics/a_basic_linux)
* [Connect to hyperion](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/1_basics/b_connecting)
* [Moving Data to hyperion](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/1_basics/c_moving_data)
* [Submit a simple Job](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/1_basics/d_simple_submit)
2. [Configuring CPU and Memory Demand](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/2_slurmconfigs)
* [Controlling Jobs](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/2_slurmconfigs/a_controlling_jobs)
* [Scaling of Demand](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/2_slurmconfigs/b_scaling_demand)
* [Configure CPU Demand](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/2_slurmconfigs/c_cpu_configs)
* [Configure Memory Demand](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/2_slurmconfigs/d_mem_configs)
3. [Advanced Topics](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/3_advanced)
* [Advanced Configurations](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/3_advanced/a_advanced_config)
* [Modules and Containers](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/3_advanced/b_modules_containers)
* [Using GPUs](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/3_advanced/c_gpu)
* [Working with Large Data](https://gitlabext.wsl.ch/wueest/hyper_tuto/-/tree/master/3_advanced/d_large_data)

The material has been developed by [Natalia Rehush](https://www.wsl.ch/en/employees/rehush.html), [Dirk Karger](https://www.wsl.ch/en/employees/karger.html), and [Rafi Wüest Karpati](https://www.wsl.ch/en/employees/wueest.html).
